let toDoList = [];
let validatorTask = new ValidatorTask();
let checkClick = false;

const TODOLIST_LOCALSTORAGE = "TODOLIST_LOCALSTORAGE";

// Covert data thành json để lưu xuống local storage
const saveLocalStorage = () => {
    let toDoListJson = JSON.stringify(toDoList);
    localStorage.setItem(TODOLIST_LOCALSTORAGE, toDoListJson);
}

// Convert json thành array và render ra giao diện khi load trang
let convertToDoListJson = localStorage.getItem(TODOLIST_LOCALSTORAGE);
if (convertToDoListJson) {
    toDoList = JSON.parse(convertToDoListJson);
    renderTaskList(toDoList);
}

// Thêm công việc mới
const addNewTask = () => {
    let isValidTask = validatorTask.checkEmpty("newTask", "alertError") && validatorTask.checkDuplicateTask("newTask", "alertError", toDoList);
    if (isValidTask) {
        let newTask = getInputValue();
        toDoList.push(newTask);
        renderTaskList(toDoList);
        saveLocalStorage();
        document.getElementById("newTask").value = "";
    }
}

// Xóa công việc
const deleteTask = (taskName) => {
    let index = findTaskIndex(toDoList, taskName);

    toDoList.splice(index, 1);
    renderTaskList(toDoList);
    saveLocalStorage();
}

// Xác nhận đã hoàn thành công việc
const completeTask = (taskName, status) => {
    let index = findTaskIndex(toDoList, taskName)

    toDoList[index].taskStatus = !status;
    renderTaskList(toDoList);
    saveLocalStorage(toDoList);
}

//Ẩn hiện toDoList hoặc completedList mỗi khi click
document.getElementById("one").addEventListener("click", () => {
    checkClick = !checkClick;
    if (checkClick) {
        document.getElementById("one").style.color = "#25b99a";
        document.getElementById("todo").style.display = "none";;
        document.getElementById("completed").style.display = "block";
        document.getElementById("completed").setAttribute("class", "todo active");
    } else {
        document.getElementById("one").style.color = "black";
        document.getElementById("completed").style.display = "none";
        document.getElementById("todo").style.display = "block";
        document.getElementById("todo").style.marginBottom = "40px";
    }
});

// Sắp xếp task theo thứ tự A-Z 
document.getElementById("two").addEventListener("click", () => {
    toDoList.sort((a, b) => a.taskName.localeCompare(b.taskName));
    renderTaskList(toDoList);
    console.log(toDoList);
});

// Sắp xếp task theo thứ tự Z-A
document.getElementById("three").addEventListener("click", () => {
    toDoList.sort((a, b) => b.taskName.localeCompare(a.taskName));
    renderTaskList(toDoList);
    console.log(toDoList);
});

// Ẩn hiện mốc thời gian đã tạo task
document.getElementById("all").addEventListener("click", () => {
    checkClick = !checkClick;
    if (checkClick) {
        toDoList.forEach((task) => {
            document.getElementById(`${task.taskName}`).style.display = "block";
        });
    } else {
        toDoList.forEach((task) => {
            document.getElementById(`${task.taskName}`).style.display = "none";
        });
    }
})

