const getInputValue = () => {
    let taskName = document.getElementById("newTask").value;
    let taskStatus = false;
    let now = new Date();
    let getDay = String(now.getDate()).padStart(2,'0');
    let getMonth = String(now.getMonth()+1).padStart(2,'0');
    let getYear = now.getFullYear();
    let getHour = String(now.getHours()).padStart(2,'0');
    let getMinute = String(now.getMinutes()).padStart(2,'0');
    // let getSecond = String(now.getSeconds()).padStart(2,'0');
    let time = `${getHour}:${getMinute}<br>${getDay}/${getMonth}/${getYear}`;
    console.log(time);
    
    return new Task(time,taskName,taskStatus);
}

const renderTaskList = (list) => {
    let contentToDoHTML = "";
    let contentCompletedHTML = "";

    list.forEach((task) => {
        if (!task.taskStatus) {
            let contentToDoTask = /*html */ `
            <li>
            <span>${task.taskName}</span>
            <span id="${task.taskName}" style="display:none">${task.time}</span>
            <span>
            <i class="fa-solid fa-trash-can" onclick="deleteTask('${task.taskName}')"></i>
            <i class="fa-solid fa-circle-check" onclick="completeTask('${task.taskName}',${task.taskStatus})"></i>
            </span>
            </li>
            `;
            contentToDoHTML += contentToDoTask;
        } else {
            let contentCompletedTask = /*html */ `
            <li>
            <span>${task.taskName}</span>
            <span id="${task.taskName}" style="display:none">${task.time}</span>
            <span>
            <i class="fa-solid fa-trash-can" onclick="deleteTask('${task.taskName}')"></i>
            <i class="fa-solid fa-circle-check" onclick="completeTask('${task.taskName}',${task.taskStatus})")></i>
            </span>
            </li>
            `;
            contentCompletedHTML += contentCompletedTask;
        }
    });

    document.getElementById("todo").innerHTML = contentToDoHTML;
    document.getElementById("completed").innerHTML = contentCompletedHTML;
}

const findTaskIndex = (array, taskName) => {
    return index = array.findIndex((task) => {
        return task.taskName == taskName;
    })
}
