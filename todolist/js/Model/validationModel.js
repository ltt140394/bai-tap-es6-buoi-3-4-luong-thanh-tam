class ValidatorTask {
    constructor() {
        this.checkEmpty = (idTarget,idErr) => {
            let valueTarget = document.getElementById(idTarget).value.trim();

            if(!valueTarget) {
                document.getElementById(idErr).innerText = "Hãy nhập 1 công việc cụ thể";
                return false;
            } else {
                document.getElementById(idErr).innerText = "";
                return true;
            }
        }

        this.checkDuplicateTask = (idTarget,idErr,array) => {
            let valueTarget = document.getElementById(idTarget).value.trim();

            let index = array.findIndex((task) => {
                return task.taskName == valueTarget;
            });

            if(index == -1) {
                document.getElementById(idErr).innerText = "";
                return true;
            } else {
                document.getElementById(idErr).innerText = "Công việc không được lặp lại";
                return false;
            }
        }
    }
}